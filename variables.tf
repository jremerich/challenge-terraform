locals {
  cluster_name = "challenge-${random_string.suffix.result}"
  lb_name = split("-", split(".", kubernetes_service.nginx.status.0.load_balancer.0.ingress.0.hostname).0).0
}
