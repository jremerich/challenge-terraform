<h1 align="center">
  CHALLENGE TERRAFORM
</h1>

<p align="center">
  <a href="https://www.terraform.io/" target="_blank" rel="noopener noreferrer"><img alt="" src="https://img.shields.io/badge/language-terraform-blue"></a> <a href="https://www.linkedin.com/in/jos%C3%A9-roberto-emerich-junior/" target="_blank" rel="noopener noreferrer"><img alt="Made by" src="https://img.shields.io/badge/made%20by-Jos%C3%A9%20Roberto%20Emerich%20Junior-blue"></a>
</p>

## 🖥️ What this project do

- Deploy a EKS cluster on `us-east-2` AWS region
- This cluster will have 3 nodes, 2 `t2.small` and 1 `t2.medium`
- A VPC named `challenge-vpc` with 3 IP's range in private and public subnets
- An IAM to provide permissions to the cluster
- 3 security groups to allow SSH access to nodes
- A kubernets deployment with 2 replicas using `nginx:1.15.8-alpine` image and a `LoadBalance` service to external access

## 🚀 Getting started to get the EKS environment ready to use:

### ☑️ Requirements

- AWS account
- aws-cli installed and configured
- S3 bucket to save the TFSTATE file (you'll need to change the bucket name on `versions.tf` file at `backend` section BEFORE `init` command)
- Terraform >= 0.14

### 📝 Follow the steps below

```bash
# Install the dependencies
$ terraform init

# Plan all needed changes
$ terraform plan -out deploy.out

# Apply all changes on your AWS account
$ terraform apply deploy.out

# To finish, clean all changes
$ terraform destroy

# Well done!
```

> Obs.: To access the nginx service use the `apply` command output `load_balancer_hostname` access our nginx.

---

Made by **José Roberto Emerich Junior** 👨‍💻 [See my linkedin](https://www.linkedin.com/in/jos%C3%A9-roberto-emerich-junior/)
